# 0.3.0.0
* rawQuery functionality
* exported boolean operations
* logging support through simple-logging
* support for quoted SQL identifiers
* IsNull and IsNotNull
* synonyms for gettable and settable fields
* true and false boolean literals
* proper Eq and Ord instances for entities
* bytea support
* numeric and date type support
* like and ilike
* Show instance for entities serializes to JSON
* toFreshEntity for converting from a database entity
* proper grouping for boolean operations in generated SQL
* support for composite types and PostgreSQL arrays
* GHC 8.2.2 support

# 0.2.0.1
* Simple documentation fixes
