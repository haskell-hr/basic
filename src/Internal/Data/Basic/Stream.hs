module Internal.Data.Basic.Stream where

import Internal.Interlude hiding (handle)

import Database.PostgreSQL.Simple
import GHC.IO.Unsafe
import Control.Exception

mvarToList :: MVar (Maybe a) -> IO [a]
mvarToList mv = do
    mby <- takeMVar mv
    case mby of
        Nothing -> return []
        Just x  -> do
            xs <- unsafeInterleaveIO (mvarToList mv)
            return (x : xs)
{-# NOINLINE mvarToList #-}

streamQuery_ :: FromRow a => Connection -> Query -> IO [a]
streamQuery_ conn q = do
    mv <- newEmptyMVar
    void $ forkIO $ handle (\BlockedIndefinitelyOnMVar -> putText "blocked") $ do
        forEach_ conn q (putMVar mv . Just)
        putMVar mv Nothing
    mvarToList mv
