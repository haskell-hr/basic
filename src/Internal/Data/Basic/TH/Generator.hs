{-|
module Internal.     : Data.Basic.TH.Types
Description : Data types and utility function used during TH generation phase
License     : MIT

This module Internal.defines functions that are used to generate Template Haskell code from AST. For AST
description take a look at 'Data.Basic.TH.Types'.
-}
module Internal.Data.Basic.TH.Generator where

import Internal.Interlude
import Internal.Data.Basic.TH.Types
import qualified Internal.Data.Basic.Types as BT
import qualified Internal.Data.Basic as B
import qualified Internal.Data.Basic.Foreign as F
import Language.Haskell.TH.Syntax as TH
import Language.Haskell.TH as TH
import Internal.Data.Basic.TH.Helper
import Internal.Data.Basic.Virtual
import Internal.Data.Basic.TH.SqlToHsTypes
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.FromRow
import GHC.Generics (Generic)
import qualified Data.Text as T

import Data.Schema.Types.Schema
import qualified Data.Schema.Types.AST as AST
import qualified Data.Schema.Types.Schema as S
import qualified Data.Schema.Types.Lens as L
-- import Data.Schema.Types.Lens hiding (field, schema)
import Data.Schema.Query hiding (getColumnType)
import qualified Data.List.NonEmpty as NE
import Database.PostgreSQL.Simple.FromField
import Database.PostgreSQL.Simple.ToField
import Data.Typeable

newEntityPrefix :: Text
newEntityPrefix = "new"

-- | Generates a data constructor for an entity
--
-- > data Post = Post { _ostId     :: Key
-- >                  , _ostName   :: Text
-- >                  , _ostUserId :: Key } deriving (Show, Read, Generic)
--
dataConstructor' :: S.Schema -> Table -> Q TH.Dec
dataConstructor' dbModel table' = do
    fields <- mfields
    return $ DataD [] entityName [] Nothing [RecC entityConstructorName fields]
        [DerivClause Nothing [ConT ''Show, ConT ''Generic]]
  where entityName = getEntityName table'
        entityConstructorName = getEntityConstructorName table'
        cols = getColumnsById' dbModel (table' ^. L.columns)
        mfields = forM cols $ \c -> do
            ct <- getColumnType dbModel table' c
            return (genColumnName table' c, Bang SourceUnpack SourceStrict, ct)

compileEntity' :: S.Schema -> Table -> [TH.Dec]
compileEntity' schema table' =
  tableInstance' schema table'
  <> [fromRowInstance' table']
  <> emptyEntity' schema table'
  <> initialAccessor' table'
  <> (concat (generateConstraintInstance schema table' <$> (table' ^. L.constraints)))
  <> (fkOptics' schema table' (table' ^. L.constraints))
  <> (virtualTables' schema table' (table' ^. L.constraints))


genColumnName :: Table -> Column -> TH.Name
genColumnName table' col =
  mkName . toS $ "_"
    <> lowerFirst (normalizeTable (table' ^. L.name))
    <> "_"
    <> normalizeName (col ^. L.name)

getEntityName :: Table -> TH.Name
getEntityName table' = mkName $ toS $ normalizeTable $ table' ^. L.name

getEntityConstructorName :: Table -> TH.Name
getEntityConstructorName table' = mkName $ toS $ normalizeTable (table' ^. L.name) <> "_Constructor"

-- | Generates a fromRow instance for a entity.
--
-- > instance FromRow Post where
-- > fromRow = Post <$> field <*> field <*> field
--
-- fromRowInstance takes a name of the entity and a list of [a],
-- which is just used to count how many fields does the entity have - nothing
-- else. It generates something similar shown in a code snippet above - a
-- working FromRow instance for that datatype.
fromRowInstance' :: Table -> TH.Dec
fromRowInstance' table' = InstanceD Nothing [] (AppT (ConT ''FromRow)
                                                                (ConT entityName))
                                     [ValD (VarP 'fromRow)
                                           (NormalB (addFields initial n)) []]
  where n = length (table' ^. L.columns)
        initial = InfixE (Just (ConE (getEntityConstructorName table')))
                         (VarE '(<$>))
                         (Just (VarE 'field))
        entityName = getEntityName table'


-- | Generates field optics for all entities
fieldOptics' :: S.Schema -> [TH.Dec]
fieldOptics' schema' = concat $ fieldOptic <$> columnNames
  where columnNames = nub $ normalizeName . (view L.name) <$> schema' ^. L.columns

-- | Generates field optics for a column
--
--
-- > name :: (FieldOptic "username" fun inType outType inVal outVal) => PolyOptic fun inType outType inVal outVal
-- > name = fieldOpticProxy (Proxy :: Proxy "username")
--
--
fieldOptic :: Text -> [Dec]
fieldOptic t = [SigD fieldName (
                           ForallT [PlainTV o]
                           [
                             AppT (ConT ''B.FieldOpticProxy)
                                  (AppT (AppT ArrowT
                                              (AppT (ConT ''Proxy)
                                                    (LitT (StrTyLit $ toS t))))
                                        (VarT o))
                           ]
                           (VarT o)),

                         ValD (VarP fieldName) (
                           NormalB (AppE (VarE 'B.fieldOpticProxy)
                                         (SigE (ConE 'Proxy)
                                               (AppT (ConT ''Proxy)
                                                     (LitT (StrTyLit $ toS t)))))) []
                       ]
  where o = mkName "o"
        fieldName = mkName $ toS t



-- |  Generates a table field instance for a column
--
-- > instance TableField Post "userId" where
-- >   type TableFieldType Post "userId" = Key
-- >   type TableFieldCapsName Post "userId" = 'Just "user_id"
-- >   tableFieldLens = ostUserId
-- >   tableFieldSqlType = Sql.Integer
--
tableField' :: S.Schema -> Table -> Column -> Q [Dec]
tableField' dbModel table' column' =
    [d|instance BT.TableField $tableNameT $fieldNameT where
        type TableFieldType $tableNameT $fieldNameT = $columnType
        type TableFieldCapsName $tableNameT $fieldNameT = 'Just $columnNameT
        tableFieldLens = $(varE lensName)
        tableFieldSqlType = $(lift (_sqlType column'))|]
  where entityName = getEntityName table'
        tableNameT = conT entityName
        fieldNameT = litT (strTyLit (toS normalName))
        columnNameT = litT (strTyLit (toS columnText))
        columnType = getColumnType dbModel table' column'

        columnText = column' ^. L.name
        normalName = normalizeName $ column' ^. L.name
        lensName = mkName $ toS ((lowerFirst.normalizeTable $ table' ^. L.name) <> "_" <> normalName)

-- | Generates required table field instances for all entities
tableFields' :: S.Schema -> Table -> Q [Dec]
tableFields' dbModel table' =
  concat <$> mapM (tableField' dbModel table') cols
  where cols = getTableColumns dbModel table'


getColumnType :: S.Schema -> Table -> Column -> Q TH.Type
getColumnType schema' table' column =
  if requiresMaybeWrapper table' column then
    AppT (ConT ''Maybe) <$> (toHsType $ column ^. L.sqlType)
  else (toHsType $ column ^. L.sqlType)

-- | Generates initial accessor for the table
--
-- > allPosts = allRows (Proxy :: Proxy "post")
--
initialAccessor' :: Table -> [TH.Dec]
initialAccessor' table' =
    [ SigD accessor
           (ForallT [PlainTV res]
                    [ConT ''B.AllRows `AppT` tableType `AppT` VarT res]
                    (VarT res))
    , FunD accessor
           [Clause []
                   (NormalB (AppE (VarE 'B.allRowsProxy)
                                  (SigE (ConE 'Proxy) (ConT ''Proxy `AppT` tableType))))
                   []] ]
    where sTableName = normalizeTable (toS tableName)
          res = mkName "res"
          accessor = mkName $ toS $ "all" <> quasyPlural sTableName
          tableName = table' ^. L.name
          tableType = ConT $ getEntityName table'


-- | Generates foreign key optics
fkOptics' :: S.Schema -> Table -> [AST.NamedConstraint TableConstraint] -> [Dec]
fkOptics' schema table' consts = concat $ fkOptic' schema table' <$> fks'
  where fks' = filter isForeignKey consts

-- | Generates foreign key optic
--
-- > author :: ForeignKeyLensProxy (Proxy "blog_post_author_fkey" -> o) => o
-- > author = foreignKeyLens @"blog_post_author_fkey"
--
fkOptic' :: S.Schema -> Table -> AST.NamedConstraint TableConstraint -> [Dec]
fkOptic' schema table' c@(AST.NamedConstraint _ (ForeignKey fromFields toTableId _)) = [
  SigD accName (ForallT [PlainTV o] [
                   AppT (ConT ''F.ForeignKeyLensProxy)
                        (AppT (ArrowT `AppT` (ConT ''Proxy `AppT` constraintTLS))
                              (VarT o))] (VarT o)),
              ValD (VarP accName)
              (NormalB (AppE (VarE 'B.foreignKeyLensProxy)
                             (SigE (ConE 'Proxy) (ConT ''Proxy `AppT` constraintTLS)))) []]
  where accName  = mkName $ toS $
            lowerFirst (toS $ nameBase $ getEntityName table')
            <> (T.concat (toList (upperFirst . normalizeName <$> fromCols)))
            <> toS (nameBase $ toEntity)
        o = mkName "o"
        constraintName = getConstraintName table' c
        constraintTLS = LitT $ StrTyLit $ toS $ constraintName
        toEntity = fromJust $ getEntityName <$> getTableById schema toTableId
        fromCols = (\(ColumnId _ columnName) -> columnName) <$> fromFields

fkOptic' _ _ _ = []


-- | Generates virtual table optics
virtualTables' :: S.Schema -> Table -> [AST.NamedConstraint TableConstraint] -> [Dec]
virtualTables' schema table' consts = concat $ virtualTable' schema table' <$> fks'
  where fks' = filter isForeignKey consts

-- | Generates virtual table optic
--
-- > posts :: VirtualTable "blog_post_author_fkey" res
-- >    => Getter' (Entity ('FromDb c) (ForeignKeyTo "blog_post_author_fkey")) res
-- > posts = virtualTableLensProxy (Proxy :: Proxy "blog_post_author_fkey")
--
virtualTable' :: S.Schema -> Table -> AST.NamedConstraint TableConstraint -> [Dec]
virtualTable' schema table' c@(AST.NamedConstraint _ (ForeignKey fromFields toTableId _)) = [
  SigD accName (ForallT [PlainTV o,PlainTV c_1] [
                    (ConT ''VirtualTable `AppT` constraintTLS) `AppT` VarT o
                    ]
                  (AppT (AppT (ConT ''B.Getter')
                              (AppT (AppT (ConT ''BT.Entity)
                                          (PromotedT 'BT.FromDb `AppT` VarT c_1))
                                    (ConT ''BT.ForeignKeyTo `AppT` constraintTLS)))
                        (VarT o))),
    ValD (VarP accName) (NormalB (AppE (VarE 'B.virtualTableLensProxy)
                                 (SigE (ConE 'Proxy) (ConT ''Proxy `AppT` constraintTLS))))
    []]
  where accName  = mkName $ toS $
            lowerFirst (toS $ nameBase $ toEntity)
            <> T.concat (toList (upperFirst . normalizeName <$> fromCols))
            <> (quasyPlural . toS . nameBase $ getEntityName table')
        o = mkName "o"
        c_1 = mkName "c1"
        constraintName = getConstraintName table' c
        constraintTLS = LitT $ StrTyLit $ toS $ constraintName
        toEntity = fromJust $ getEntityName <$> getTableById schema toTableId
        fromCols = (\(ColumnId _ columnName) -> columnName) <$> fromFields
virtualTable' _ _ _ = []


generateConstraintInstance :: S.Schema -> Table -> AST.NamedConstraint TableConstraint -> [Dec]
-- | Generates unique key constraint instance
--
-- > instance UniqueConstraint "blog_user_pkey" where
-- >    type UniqueTable "blog_user_pkey" = User
-- >    type UniqueFields "blog_user_pkey" = '["id"]
--
generateConstraintInstance _ table' c@(AST.NamedConstraint _ (Unique fields)) = [
  InstanceD Nothing [] (
      AppT (ConT ''BT.UniqueConstraint) (LitT (StrTyLit keyName))
      ) [
      TySynInstD (TySynEqn Nothing
        (AppT (ConT ''BT.UniqueTable) (LitT (StrTyLit keyName)))
        (ConT entityName)),
      TySynInstD (TySynEqn Nothing
        (AppT (ConT ''BT.UniqueFields) (LitT (StrTyLit keyName)))
        (listToTypeLevel cols))]
  ]
  where keyName = toS $ getConstraintName table' c
        entityName = getEntityName table'
        cols = (LitT . StrTyLit . toS)
          <$> (\(ColumnId _ columnName) -> normalizeName columnName) <$> fields
-- | Generates primary key instance
--
-- > instance PrimaryKeyConstraint "blog_user_pkey"
--
generateConstraintInstance _ table' c@(AST.NamedConstraint _ (PrimaryKey fields)) = [
  InstanceD Nothing [] (
      AppT (ConT ''BT.UniqueConstraint) (LitT (StrTyLit keyName))
      ) [
      TySynInstD (TySynEqn Nothing
        (AppT (ConT ''BT.UniqueTable) (LitT (StrTyLit keyName)))
        (ConT entityName)),
      TySynInstD (TySynEqn Nothing
        (AppT (ConT ''BT.UniqueFields) (LitT (StrTyLit keyName)))
        (listToTypeLevel cols))],
  InstanceD Nothing [] (AppT (ConT ''BT.PrimaryKeyConstraint) (LitT (StrTyLit $ keyName))) []
  ]
  where keyName = toS $ getConstraintName table' c
        entityName = getEntityName table'
        cols = (LitT . StrTyLit . toS)
          <$> (\(ColumnId _ columnName) -> normalizeName columnName) <$> fields
-- | Generates foreign key instance
--
-- > instance ForeignKeyConstraint "blog_post_author_fkey" where
-- > type ForeignKeyFrom "blog_post_author_fkey" = Post
-- > type ForeignKeyFromFields "blog_post_author_fkey" = '["author"]
-- > type ForeignKeyTo "blog_post_author_fkey" = User
-- > type ForeignKeyToFields "blog_post_author_fkey" = '["id"]
--
generateConstraintInstance schema table' c@(AST.NamedConstraint _ (ForeignKey fromFields toTableId toFields)) =
  [  InstanceD Nothing [] (ConT ''BT.ForeignKeyConstraint `AppT` constraintTLS) [
        TySynInstD $ TySynEqn Nothing (AppT (ConT ''BT.ForeignKeyFrom) constraintTLS) fromEntityType,
        TySynInstD $ TySynEqn Nothing (AppT (ConT ''BT.ForeignKeyFromFields) constraintTLS) (listToTypeLevel fromCols),
        TySynInstD $ TySynEqn Nothing (AppT (ConT ''BT.ForeignKeyTo) constraintTLS) toEntityType,
        TySynInstD $ TySynEqn Nothing (AppT (ConT ''BT.ForeignKeyToFields) constraintTLS) (listToTypeLevel toCols)
        ]
  ]
  where constraintName = getConstraintName table' c
        constraintTLS = LitT $ StrTyLit $ toS $ constraintName
        fromEntityType = ConT $ getEntityName table'
        toEntityType = ConT $ fromJust $ getEntityName <$> getTableById schema toTableId
        fromCols = (LitT . StrTyLit . toS)
          <$> (\(ColumnId _ columnName) -> normalizeName columnName) <$> fromFields
        toCols = (LitT . StrTyLit . toS)
          <$> (\(ColumnId _ columnName) -> normalizeName columnName) <$> toFields
generateConstraintInstance _ _ _ = []

-- | Generates an Table instance for a table in schema
--
-- > instance Table Post where
-- >     type TableName Post = "blog_post"
-- >     type TableFields Post = ["id", "name", "author"]
-- >     type TableConstraints Post = '[]
-- >     type TablePrimaryKey Post = 'Nothing
-- >     type TableRequiredFields Post = ['DynamicDefault 'id, 'Required "name", 'Required "author"]
--
tableInstance' :: S.Schema -> Table -> [Dec]
tableInstance' schema table' = [
  InstanceD Nothing [] (ConT ''BT.Table `AppT` entityType) [
      TySynInstD $ TySynEqn Nothing (AppT (ConT ''BT.TableName) entityType) (LitT $ StrTyLit tableName),
      TySynInstD $ TySynEqn Nothing (AppT (ConT ''BT.TableFields) entityType) entityFieldTypes,
      TySynInstD $ TySynEqn Nothing (AppT (ConT ''BT.TableConstraints) entityType) entityConstraints,
      TySynInstD $ TySynEqn Nothing (AppT (ConT ''BT.TablePrimaryKey) entityType) pkTypes,
      TySynInstD $ TySynEqn Nothing (AppT (ConT ''BT.TableRequiredFields) entityType) (listToTypeLevel fields),
      ValD (VarP 'BT.newEntity) (coerceBody' table') []
      ]
  ]
  where entityType = ConT $ getEntityName table'
        tableName = toS $ table' ^. L.name
        entityFields = normalizeName . view L.name <$> getTableColumns schema table'
        entityFieldTypes = listToTypeLevel $ LitT . StrTyLit . toS <$> entityFields
        entityConstraints = listToTypeLevel $
              fmap (ConT 'BT.Unique `AppT`) $ LitT . StrTyLit . toS
          <$> getConstraintName table'
          <$> filter (\c -> isUnique c || isPrimaryKey c) (table' ^. L.constraints)
        primaryKeys = getConstraintName table' <$> filter (isPrimaryKey) (table' ^. L.constraints)
        pkTypes = if null primaryKeys
          then ConT 'Nothing
          else
            AppT (ConT 'Just) . LitT . StrTyLit . toS $ fromJust $ head primaryKeys
        allCols = getTableColumns schema table'
        (requiredCols, optionalCols) = partition (isColumnRequired table') allCols
        fields = required' requiredCols <> dynamicDefault' optionalCols

partFromCols :: NE.NonEmpty S.ColumnId -> Text
partFromCols cols = T.intercalate "_" $ NE.toList (fmap S._columnName cols)

getConstraintName :: Table -> AST.NamedConstraint TableConstraint -> Text
getConstraintName table' (AST.NamedConstraint cname (S.PrimaryKey _)) =
  maybe (table' ^. L.name <> "_pkey") toS cname
getConstraintName _ (AST.NamedConstraint cname (S.NotNull _)) =
  fromMaybe "_nn_" cname
getConstraintName _ (AST.NamedConstraint cname (S.Null _)) =
  fromMaybe "_null_" cname
getConstraintName table' (AST.NamedConstraint cname (S.Unique cids)) =
  fromMaybe (table' ^. L.name <> "_" <> partFromCols cids <> "_key") cname
getConstraintName table' (AST.NamedConstraint cname (S.Default col _)) =
  fromMaybe (table' ^. L.name <> "_" <> S._columnName col <> "_default") cname
getConstraintName table' (AST.NamedConstraint cname (S.ForeignKey fromCols toTable toCols)) =
  fromMaybe (table' ^. L.name <> "_" <> fromPart <> "_" <> S._tableName toTable <> "_" <> toPart <> "_fkey") cname
  where
  fromPart = partFromCols fromCols
  toPart = partFromCols toCols
getConstraintName table' (AST.NamedConstraint cname (S.Check col _)) =
  fromMaybe (table' ^. L.name <> "_" <> S._columnName col <> "_check") cname



-- | Generates a empty entity
--
-- > newPost :: Entity ('Fresh ['DynamicDefault "id", 'Required "name", 'Required "author"])
-- > newPost = Entity (Post 1 "" 1)
--
emptyEntity' :: S.Schema -> Table -> [Dec]
emptyEntity' schema table' = [
  SigD fname (AppT (AppT (ConT ''BT.Entity)
                         (PromotedT 'BT.Fresh `AppT` listToTypeLevel fields))
                   (ConT (getEntityName table'))),
    ValD (VarP fname) (coerceBody' table') []]
  where fname = mkName $ toS $ newEntityPrefix <> normalizeTable (table' ^. L.name)
        allCols = getTableColumns schema table'
        (requiredCols, optionalCols) = partition (isColumnRequired table') allCols
        fields = required' requiredCols <> dynamicDefault' optionalCols


-- | Applies 'Required' constraint to list of columns
required' :: [Column] -> [TH.Type]
required' cs = f <$> cs
  where f ci = ConT 'BT.Required `AppT` (LitT . StrTyLit . toS . normalizeName $ ci ^. L.name)

-- | Applies 'DynamicDefault' constraint to a list of columns
dynamicDefault' :: [Column] -> [TH.Type]
dynamicDefault' cs = f <$> cs
  where f ci = ConT 'BT.DynamicDefault `AppT` (LitT . StrTyLit . toS . normalizeName $ ci ^. L.name)

-- | Generates coerce body. See 'NullValue' why this is needed
coerceBody' :: Table -> TH.Body
coerceBody' table' = NormalB (AppE (ConE 'BT.Entity ) (nullValue' n $ ConE name'))
  where n = length $ table' ^. L.columns
        name' = getEntityConstructorName table'


nullValue' :: Int -> TH.Exp -> TH.Exp
nullValue' 0 initial = initial
nullValue' n initial = AppE (nullValue' (n - 1) initial) (VarE 'nullValue)

-- | Generated the value representing the schema that can be applied to the database
--

-- > initialSchema :: Schema
-- > initialSchema = Schema "CREATE...
schemaValue :: Text -> Text -> [Dec]
schemaValue name' text =
    [ SigD sname (ConT ''Internal.Data.Basic.TH.Types.Schema)
    , ValD (VarP sname) (NormalB (ConE 'Internal.Data.Basic.TH.Types.Schema `AppE` LitE (StringL (toS text)))) [] ]
    where
    sname = mkName (toS name' <> "Schema")

generateCustomTypes :: S.Schema -> Q [Dec]
generateCustomTypes schema' = concat <$> mapM (generateCustomType schema') (schema' ^. L.types)

enumToString :: (Show a, Eq a) => [(a, String)] -> a -> String
enumToString table name' = case lookup name' table of
    Just sql -> sql
    Nothing -> error ("Unexpected enum variant " <> show name' <> ". Haskell and SQL enums are mismatched.")

stringToEnum :: [(a, String)] -> String -> a
stringToEnum table sql = case find ((== sql) . snd) table of
    Just (x, _) -> x
    Nothing -> error ("Unexpected enum text in db: " <> show sql <> ". Haskell and SQL enums are mismatched.")


readFromField :: (Read a, Typeable a) => String -> [(a, String)] -> Field -> Maybe ByteString -> Conversion a
readFromField enumName table f mdata = do
  tname <- typename f
  if normalizeTable (toS tname) /= toS enumName
    then returnError Incompatible f ""
    else
      case mdata of
        Nothing  -> returnError UnexpectedNull f ""
        Just dat -> return (stringToEnum table (toS dat))

generateCustomType :: S.Schema -> (TableId, AST.CustomType) -> Q [Dec]
generateCustomType schema' (TableId _ name',  (AST.Enumerated constructors)) =
  let enumName = normalizeTable name'
      enumNameString = toS enumName :: String
      thEnumName = mkName $ toS enumName
      table = (\c -> (mkName $ toS $ normalizeTable c, toS c :: String)) <$> constructors
      reifiedTable = ListE <$> mapM (\(n, t) -> [|($(conE n), t)|]) table
      firstConstructor = case constructors of
        [] -> VarE (mkName "undefined")
        (c : _) -> ConE (mkName (toS (normalizeTable c)))
      dataDef = DataD
        []
        thEnumName
        []
        Nothing
        ((\(n, _) -> NormalC n []) <$> table )
        [ DerivClause (Just StockStrategy) [ConT ''Show, ConT ''Generic, ConT ''Eq, ConT ''Ord, ConT ''Read]
        , DerivClause (Just AnyclassStrategy) [ConT ''FromJSON, ConT ''ToJSON] ]
  in
    (dataDef :)
    <$> [d|
      instance NullValue $(TH.conT thEnumName) where nullValue = $(return firstConstructor)
      instance FromField $(TH.conT thEnumName) where fromField = readFromField enumNameString $reifiedTable
      instance ToField $(TH.conT thEnumName) where
        toField = toField . enumToString $reifiedTable |]
generateCustomType schema' _ = undefined
