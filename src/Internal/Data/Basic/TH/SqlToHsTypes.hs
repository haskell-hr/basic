-----------------------------------------------------------------------------
-- |
-- Module      : Internal.Data.Basic.TH.SqlToHsTypes
-- Copyright   :  (c) Nikola Henezi, Luka Horvat
-- License     :  MIT
--
-- Maintainer  :  nikola@henezi.com, luka.horvat9@gmail.com
--
-- Conversion between Postgres types and haskell datatypes. As a starting point,
-- 'fromField' from Database.PostgreSQL.Simple.FromField is used.
-- If you need support for additional datatypes, please open an issue
-- (https://gitlab.com/haskell-hr/basic) or email maintainers
-----------------------------------------------------------------------------
module Internal.Data.Basic.TH.SqlToHsTypes (toHsType) where

import           Data.Time                     (LocalTime, TimeOfDay, Day, ZonedTime)
import           Internal.Interlude            hiding (Type)
import           Language.Haskell.TH           hiding (Name)
import           Data.Scientific
import           Data.UUID
import           Internal.Data.Basic.SqlToHsTypes
import           Database.PostgreSQL.Simple.Types
import           Internal.Data.Basic.Types
import           Internal.Data.Basic.TH.Helper
import qualified Data.Schema.Types.Schema as Schema

type FreshEntity = Entity ('Fresh '[])

-- | Actual conversions. The following Postgres types are currently supported:
-- Still missing support for:
-- * monetary types (depends on postgresql settings, lc_monetary)
-- * interval date type - complex data parsing
-- * composite types
-- * range types
toHsType :: Schema.SqlType -> Q Type
    ----------------------------
    ---- Numeric data types ----
    ----------------------------
toHsType (Schema.Smallint) = return $ ConT ''Int
toHsType (Schema.Integer) = return $ ConT ''Int
toHsType (Schema.Bigint) = return $ ConT ''Int
toHsType (Schema.Real) = return $ ConT ''Double
toHsType (Schema.Double) = return $ ConT ''Double
toHsType (Schema.Numeric _ _) = return $ ConT ''Scientific
    ------------------------------
    ---- Character data types ----
    ------------------------------
toHsType (Schema.Character _ _) = return $ ConT ''Text
toHsType (Schema.Text) = return $ ConT ''Text
    ---------------------------
    ---- Binary data types ----
    ---------------------------
toHsType (Schema.Bytea) = return $ ConT ''ByteString
    ------------------------------
    ---- Date/time data types ----
    ------------------------------
toHsType (Schema.Date) = return $ ConT ''Day
toHsType (Schema.Timestamp _ False) = return $ ConT ''LocalTime
toHsType (Schema.Timestamp _ True) = return $ ConT ''ZonedTime
toHsType (Schema.Time _ False) = return $ ConT ''TimeOfDay
toHsType (Schema.Time _ True) = return $ ConT ''Text -- @TODO figure out the best conversion
    ----------------------------
    ---- Boolean data types ----
    ----------------------------
toHsType (Schema.Boolean) = return $ ConT ''Bool
    ------------------------------
    ---- Geometric data types ----
    ------------------------------
toHsType (Schema.Point) = return $ ConT ''Point -- AppT (AppT (TupleT 2) (ConT ''Double)) (ConT ''Double)
-- toHsType (Schema.Line) = return $ AppT (AppT (TupleT 3) (ConT ''Double)) (ConT ''Double)
    ------------------------------
    ----- Other types  -----------
    ------------------------------
toHsType (Schema.Uuid) = return $ ConT ''UUID
toHsType (Schema.Json) = return $ ConT ''Value
toHsType (Schema.Jsonb) = return $ ConT ''Value
toHsType (Schema.Array dim t) = toHsType t >>= \t' ->
    return $ iterate (ConT ''PGArray `AppT`) t' !! dim
toHsType (Schema.CompositeType (Schema.TableId _ tab)) = do
    let n = mkName (toS (normalizeTable tab))
    return $ ConT ''FreshEntity `AppT` ConT n
toHsType (Schema.EnumType (Schema.TableId _ tab) _) = do
    let n = mkName (toS (normalizeTable tab))
    return $ ConT n
toHsType t = error $ "toHsType: Unsupported SQL type: " <> show t
