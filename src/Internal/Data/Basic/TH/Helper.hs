{-|
module Internal.     : Data.Basic.TH.Types
Description : Data types and utility function used during TH generation phase
License     : MIT

This module Internal.contains helper functions that are used in code generation process.
-}
module Internal.Data.Basic.TH.Helper where

import Internal.Interlude

import Cases
import Database.PostgreSQL.Simple.FromRow

import qualified Data.Text as T
import Control.Effects.Signal
import Language.Haskell.TH.Syntax as TH
import Data.Void

type Throws e = MonadEffect (Signal e Void)

-- | lifts a list of types to a type level (i.e. typelevel list)
--   e.g. `listToTypeLeve [Int, String, Double] = '[ Int, String, Double ]`
listToTypeLevel :: Foldable f => f TH.Type -> TH.Type
listToTypeLevel = foldr (\t le -> AppT (AppT PromotedConsT t) le) PromotedNilT


-- | Used to append n times `<$> field` to the expression
addFields :: TH.Exp -> Int -> TH.Exp
addFields e n
  | n > 1 = addField (addFields e (n - 1))
  | otherwise = e

-- | Used to append `<$> field` at the end of the expression
addField :: TH.Exp -> TH.Exp
addField a = InfixE (Just a) (VarE '(<*>)) (Just $ VarE 'field)

-- | returns a plural of a known noun.
--   Basically just appends `s` or `es`
quasyPlural :: Text -> Text
quasyPlural s = if T.null s then "" else case T.last s of
  's' -> s <> "es"
  _ -> s <> "s"

lowerFirst :: Text -> Text
lowerFirst t = fromMaybe t (f <$> T.uncons t)
    where f (a, b) = T.cons (toLower a) b

lowerAll :: Text -> Text
lowerAll = T.map toLower

upperFirst :: Text -> Text
upperFirst t = fromMaybe t (f <$> T.uncons t)
    where f (a, b) = T.cons (toUpper a) b

-- | Converts camel case or snake case to normal form. Examples:
--
-- > normalizeName "ABCDE"
-- "abcde"
-- > normalizeName "ABC_DE"
-- "abcDe"
-- > normalizeName "AbcDe"
-- "abcDe"
-- > normalizeName "abcDe"
-- "abcDe"
-- > normalizeName "abcde"
-- "abcde"
normalizeName :: Text -> Text
normalizeName =
      lowerFirst
    . camelize
    . mconcat
    . fmap (\g -> if isUpper (T.head g) then upperFirst (T.map toLower g) else g)
    . T.groupBy (\c1 c2 -> isUpper c1 && isUpper c2)

-- Our convention for "standarnized names"
-- @TODO What if users have their own naming scheme?
normalizeTable :: Text -> Text
normalizeTable = upperFirst . normalizeName

-- | Generates name for the column lens (i.e. lowercases first letter)
columnNameToLensName :: Text -> Text
columnNameToLensName = lowerFirst
