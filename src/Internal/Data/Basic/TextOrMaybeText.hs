module Internal.Data.Basic.TextOrMaybeText where

import Data.Text
import Prelude


class TextOrMaybeText a where

instance {-# OVERLAPPABLE #-} a ~ Text => TextOrMaybeText a where

instance b ~ Text => TextOrMaybeText (Maybe b) where
